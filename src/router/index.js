import { createRouter, createWebHistory } from '@ionic/vue-router';

const routes = [
  {
    path: '',
    redirect: '/login',
    component: () => import ('../layouts/AuthLayout.vue'),
    children: [
      {
        path: '/login',
        component: () => import ('../views/auth/Index.vue')
      }
    ]
  },
  {
    path: '/folder/:id',
    component: () => import ('../views/Folder.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
