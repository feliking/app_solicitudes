import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'app_solicitudes',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
